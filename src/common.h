#ifndef __INCLUDED_COMMON_H__
#define __INCLUDED_COMMON_H__

const int PLAYER_PRIORITY = 50000;
const int ANIMATION_PRIORITY = 1000000;

#pragma comment(lib, "DxLib.lib")

#include <Windows.h>
#include <tchar.h>
#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <cassert>

struct EVT {
	enum {
		NONE = 0,
		START,
		ENDING,
		BATTLEMODE,
		ADVENTUREMODE,
		CANCEL,
		QUIT,
		LENGTH
	};
};



#ifdef _DEBUG
#define OutputDebugStringF(str, ...) \
	{ \
		TCHAR c[256]; \
		_stprintf_s(c, str, __VA_ARGS__); \
		OutputDebugString(c); \
	}

#else 
#define OutputDebugStringF(str, ...)
#endif

void createConsoleWindow();
void closeConsoleWindow();

#endif // !__INCLUDED_COMMON_H__
