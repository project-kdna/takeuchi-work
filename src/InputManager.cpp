#include "InputManager.h"
using namespace kdna;


/*! @brief キーの割り当て
@param[in] inputKey 入力キー
@param[in] configKey 割り当てるキー
*/
bool InputManager::assignKey(int inputKey, int configKey) {
	assert(mp_configKeyMap && "Undefined configKeyMap from assignKey()");
	// そのままConfigKeyMapに投げる
	return mp_configKeyMap->assignKey(inputKey, configKey);
}


/*! @brief ゲームパッドの入力ボタンの割り当て
@param[in] inputPad ゲームパッドの入力ボタン
@param[in] configPad 割り当てるキー
*/
bool InputManager::assignPad(int inputPad, int configPad) {
	assert(mp_configKeyMap && "Undefined configKeyMap from assignPad()");
	// そのままConfigKeyMapに投げる
	return mp_configKeyMap->assignPad(inputPad, configPad);
}


/*! @brief @brief デフォルトのキーを割り当てる
*/
bool InputManager::loadKeyMap() {
	assert(mp_configKeyMap && "Undefined configKeyMap from loadKeyMap()");
	// そのままConfigKeyMapに投げる
	return mp_configKeyMap->loadKeyMap();

}


/*! @brief ファイル(keymap)からキーを割り当てる
@param[in] keymap キーマップコンフィグのバイナリデータ
*/
bool InputManager::loadKeyMap(char* keymap) {
	assert(mp_configKeyMap && "Undefined configKeyMap from loadKeyMap()");
	// そのままConfigKeyMapに投げる
	return mp_configKeyMap->loadKeyMap(keymap);

}


/*! @brief 割り当てたキーの状態を取得
@param[in] configKey 割り当てたキー
@param[in] isTrigger 長押しフラグ(Trueで長押し)
@return キー入力フレーム数(0: 入力なし)
*/
int InputManager::getConfigKeyState(int configKey, bool isTrigger) {
	assert(mp_configKeyMap && "Undefined configKeyMap from getConfigKeyState()");
	int idxKey = mp_configKeyMap->getInputKey(configKey);
	int idxPad = mp_configKeyMap->getInputPad(configKey);
	int counter = cmpInteger(mp_keyCounter[idxKey], mp_PadCounter[idxPad]);
	
	if (isTrigger == false) {
		// 押した瞬間だけ0以外を返す
		return (counter == 1);
	}
	// 押したフレーム数を返す
	return counter;
}


/*! @brief 状態を更新する(キーボード)
*/
bool InputManager::updateKeyboard() {
	// 入力更新
	char keyBuffer[256];
	if (GetHitKeyStateAll(keyBuffer) != 0) { return false; }

	for (int i = 0; i < 256; i++)
	{
		if (keyBuffer[i]) {
			mp_keyCounter[i]++;
		}
		else {
			mp_keyCounter[i] = 0;
		}
	}
	return true;
}


/*! @brief 状態を更新する(マウス)
*/
bool InputManager::updateMouse() {
	if (GetMousePoint(&m_mx, &m_my)){
		return false;
	}
	m_mInput = GetMouseInput();

	return true;
}


/*! @brief 状態を更新する(ゲームパッド)
*/
bool InputManager::updateGamepad() {
	int padBuffer;
	int mul = 1;

	padBuffer = GetJoypadInputState(DX_INPUT_PAD1);
	for (int i = 0; i<PAD_LENGTH; i++){
		if (padBuffer & mul){
			mp_PadCounter[i]++;
		}
		else{
			mp_PadCounter[i] = 0;
		}
		mul <<= 1;
	}

	return true;
}


/*! @brief 状態を更新する(すべて)
*/
bool InputManager::updateAll() {
	bool k, m, g;

	k = updateKeyboard();
	m = updateMouse();
	// TODO: ゲームパッドが有効か調べてから
	if (GetJoypadNum() >= 1){
		g = updateGamepad();
	}
	else{
		g = true;
	}

	return k && m && g;
}