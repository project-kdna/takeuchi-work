#include <stdio.h>
#include "ConfigReader.h"

using namespace kdna;


/*! @brief ファイルを読み込み，コンフィグデータを詰める
@param[in] filename コンフィグファイル
*/
bool ConfigReader::load(const char* filename) {
	FILE *fp;

	if(fopen_s(&fp, filename, "rb") != 0) return false;
	fread_s(mp_keyMap, KEY_MAP_LENGTH, sizeof(char), KEY_MAP_LENGTH, fp);
	
	fclose(fp);
	return true;
}