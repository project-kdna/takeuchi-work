#include "RootScene.h"

/*! @brief コンストラクタ */
RootScene::RootScene() : Scene(NULL)
{
	// シーンを有効にする
	validate();
	// スタートイベントを投げる
	mp_scene = receiveMessage(this, EVT::START);
}

/*! @brief デストラクタ */
RootScene::~RootScene()
{
	// 子供シーンを全て削除する
	destroy();
}

/*! @brief シーンを実行する */
Scene* RootScene::run()
{
	Scene* next = mp_scene->run();
	mp_scene->draw();

	// シーンが切り替わったなら切り替える
	if (next != mp_scene) {
		mp_scene = next;
	}
	
	return mp_scene;
}

/*! @brief 描画する */
void RootScene::draw() {}

/*! @brief 初期化する */
void RootScene::initialize()
{
	// 子供リストの初期化
	if (mp_childs.size() == 0) {
		mp_childs = std::vector<Scene*>(SC::ROOT::LENGTH);

		// シーンを生成
		// TODO: ファクトリクラスに投げること
		mp_childs[SC::ROOT::TITLE] = new TitleScene(this);
		//mp_childs [SC::ROOT::ENDING] = new EndingScene(this);
	}
	
	OutputDebugStringF("RootScene: 初期化しました\n");
}

/*! @brief 子供からRootSceneに送られたイベントを処理する
	@param[in] caller 呼び元シーン
	@param[in] message イベントメッセージ
	@return イベントに対するシーン
*/
Scene* RootScene::processMessage(Scene* caller, int message)
{
	Scene* next = 0;
	
	switch (message)
	{
	case EVT::START:
	case EVT::CANCEL:
		next = mp_childs[SC::ROOT::TITLE];
		break;

	case EVT::NONE:
		next = caller;	// イベントがないときの処理
		break;

	case EVT::QUIT:
		next = NULL;	// 終了

	default:
		assert("イベントが存在しません\n");
		next = NULL;	// 異常強制終了
		break;
	}

	return next;
}