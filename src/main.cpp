
#include "RootScene.h"
#include "InputManager.h"
#include "DrawManager.h"
#include "common.h"

#include <DxLib.h>

using namespace kdna;

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
#ifdef _DEBUG
	createConsoleWindow();
#endif

	ChangeWindowMode(true);
	DxLib_Init();
	SetDrawScreen(DX_SCREEN_BACK);
	
	// 描画マネージャ
	DrawManager* dmgr = DrawManager::getInstance();

	// 入力マネージャ
	InputManager &idev = InputManager::getInstance();

	// タスクの登録
	//TaskManager* tmgr = TaskManager::getInstance();
	//tmgr->append(new DemoTask());

	// シーンの生成
	RootScene scene;

	while (true)
	{
		if (ProcessMessage()) { break; }
		ClearDrawScreen();
		idev.updateAll();

		// タスクの実行
		//tmgr->run();

		// シーンの実行
		if (scene.run() == nullptr) { break; }
		
		// 描画
		dmgr->draw();

		dmgr->clearAll();
		ScreenFlip();
	}

	printf("正常終了しました！\n");
	DxLib_End();

	DrawManager::getInstance()->deleteInstance();
	

#ifdef _DEBUG
	closeConsoleWindow();
#endif

	return 0;
}