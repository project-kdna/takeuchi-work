#ifndef __INCLUDED_INPUTMANAGER_H__
#define __INCLUDED_INPUTMANAGER_H__

#include <vector>
#include <DxLib.h>
#include <cassert>
#include "ConfigKeyMap.h"


namespace kdna {
/*! @brief 入力デバイス管理クラス
	入力デバイス(マウス、キーボード、ゲームパッドなど)の入力を監視するシングルトンクラス。
	configKeyMapを登録することで、キーコンフィグにも対応する。
	*/
class InputManager {
public:
	/*! @brief デストラクタ
	*/
	virtual ~InputManager(){
		delete[] mp_keyCounter;
		delete[] mp_PadCounter;
		delete mp_configKeyMap;
	};
	
	/*! @brief シングルトン生成
	*/
	static InputManager& getInstance() {
		static InputManager input_manager;
		return input_manager;
	}

	/*! @brief キーの生の状態を取得
	@param[in] inputKey 入力キー
	*/
	int getKeyState(int inputKey) { return mp_keyCounter[inputKey]; }

	/*! @brief ゲームパッドのボタンの生の状態を取得
	@param[in] inputPad 入力キー
	*/
	int getPadState(int inputPad) { return mp_PadCounter[inputPad]; }

	int getConfigKeyState(int configKey, bool isTrigger = true);

	bool assignKey(int inputKey, int configKey);
	bool assignPad(int inputPad, int configKey);

	bool loadKeyMap();
	bool loadKeyMap(char* keymap);

	bool updateAll();

private:
	// 外部からのインスタンス生成を禁止
	InputManager() {
		mp_configKeyMap = NULL;
		mp_keyCounter = new int[256];
		mp_PadCounter = new int[PAD_LENGTH];

		// キーマップの生成
		createConfigKeyMap();
		loadKeyMap();
	}

	// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)
	InputManager(const InputManager& c);	
	InputManager& operator =(const InputManager& c);

	// コンフィグキーマップの生成
	void createConfigKeyMap() {
		if (!mp_configKeyMap) {
			mp_configKeyMap = new ConfigKeyMap();
		}
	}

	// 個々のアップデート関数
	bool updateKeyboard();
	bool updateMouse();
	bool updateGamepad();

	// 数字比較(大きい物を返す)
	int cmpInteger(int a, int b) { return (a > b) ? a : b; };

	// メンバ変数
	int* mp_keyCounter;		//!< DXLib: 長押しの検出
	int* mp_PadCounter;		//!< DXLib: 
	int m_mx;				//!< DxLib: マウスX座標
	int m_my;				//!< DxLib: マウスY座標
	int m_mInput;			//!< DxLib: マウス入力
	ConfigKeyMap* mp_configKeyMap;		//!< キーマップクラス
};


};
#endif // !__INCLUDED_INPUTMANAGER_H__




