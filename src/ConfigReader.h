#ifndef __INCLUDED_CONFIGREADER_H__
#define __INCLUDED_CONFIGREADER_H__

#include "ConfigKeyMap.h"

#define KEY_MAP_LENGTH 2*KEY_LENGTH

namespace kdna{
/*! @brief コンフィグ読み込みクラス
コンフィグファイル(バイナリ)を読み込み、
*/
class ConfigReader {
public:
	/*! @brief ファイルを読み込み，コンフィグデータを詰める
	@param[in] filename コンフィグファイル
	*/
	bool load(const char* filename);

	/*! @brief ファイルを読み込み，コンフィグデータを詰める
	@param[in] filename コンフィグファイル
	*/
	bool load();

	/*! @brief キーマップのコンフィグを取得
	*/
	char* getKeyMap() { return mp_keyMap; };

private:
	char mp_keyMap[KEY_MAP_LENGTH];
};

};


#endif