#include "DrawManager.h"

/* static初期化 */
DrawManager* DrawManager::mp_inst = 0;

/*! @brief コンストラクタ */
Image::Image(int handle, double alpha, int x, int y)
{
	int w, h;
	GetGraphSize(m_handle, &w, &h);

	assert(m_handle != -1 && "画像がありません");
	m_handle = handle;
	m_x = x;
	m_y = y;
	m_width = w;
	m_height = h;
	m_alpha = alpha;
}

/*! @brief デストラクタ */
Image::~Image() {}

/*! @brief 画像を描画 */
void Image::draw()
{
	// 画像を描画する
	if (m_alpha > 0.0) {
		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

		DrawGraph(m_x, m_y, m_handle, true);

		if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
	}
}



/*! @brief コンストラクタ */
Layer::Layer()
{
	m_alpha = 255.0;
	m_isVisible = true;
}

/*! @brief デストラクタ */
Layer::~Layer() {}

/*! @brief 画像を描画リストに積む
	@param[in] img 描画したい画像オブジェクト */
void Layer::append(Image* img)
{
	mp_images.push_back(img);
}

/*! @brief 描画リストから画像を削除
	@param[in] img 削除したい画像オブジェクト */
void Layer::remove(Image* img)
{
	for (ImgItr it = mp_images.begin(); it != mp_images.end(); ++it)
	{
		if ((*it) == img) {
			it = mp_images.erase(it);
		}
	}
}

/*! @brief 描画リストのクリア */
void Layer::clear()
{
	// 描画リストのクリア
	mp_images.clear();
}

/*! @brief レイヤを描画する */
void Layer::draw()
{
	// 登録されたImageを描画する
	std::vector<Image*>::iterator it;
	for (it = mp_images.begin(); it != mp_images.end(); ++it)
	{
		if (m_alpha > 0.0 && m_isVisible == true)
		{
			if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_ALPHA, (int)m_alpha); }

			(*it)->draw();

			if (m_alpha < 255.0) { SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0); }
		}
	}
}



/*! @brief コンストラクタ(private) */
DrawManager::DrawManager()
{
	// ある程度確保しておく
	mp_layers.resize(MAX_LAYER_NUM);
}

/*! @brief デストラクタ */
DrawManager::~DrawManager() {}

/*! @brief レイヤーの描画リストに追加 */
void DrawManager::append(Image* img, int layerIdx)
{
	assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");

	// 初期化されていなければ新しくレイヤーを生成する
	if (mp_layers[layerIdx] == NULL) {
		mp_layers[layerIdx] = new Layer();
	}

	// 描画予定項目の追加
	mp_layers[layerIdx]->append(img);
	mp_layers[layerIdx]->setAlpha(255.0);
	mp_layers[layerIdx]->setIsVisible(true);
}

/*! @brief レイヤーの描画リストから削除 */
void DrawManager::remove(Image* img, int layerIdx)
{
	assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");

	// レイヤーにお願いする
	if (mp_layers[layerIdx] != NULL) { mp_layers[layerIdx]->remove(img); }
}

/*! @brief レイヤーの描画リストを全て削除する */
void DrawManager::clear(int layerIdx)
{
	assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");

	// レイヤーにお願いする
	if (mp_layers[layerIdx] != NULL) { mp_layers[layerIdx]->clear(); }
}

/*! @brief 全てのレイヤーの描画リストを全て削除する */
void DrawManager::clearAll()
{
	for (LayerItr it = mp_layers.begin(); it != mp_layers.end(); ++it)
	{
		// レイヤーにお願いする
		if ((*it) != NULL) { (*it)->clear(); }
	}
}

/*! @brief 描画する */
void DrawManager::draw()
{
	// 描画する
	std::vector<Layer*>::iterator it;

	for (it = mp_layers.begin(); it != mp_layers.end(); ++it)
	{
		if ((*it) != NULL) { (*it)->draw(); }
	}
}