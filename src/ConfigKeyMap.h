#ifndef __INCLUDED_CONFIGKEYMAP_H__
#define __INCLUDED_CONFIGKEYMAP_H__

#include "common.h"
#include <vector>
#include <DxLib.h>
#include <cassert>


namespace kdna {
// 仮想ボタンの定義
enum GAMEKEY {
	KEY_NONE,
	KEY_OK,
	KEY_CANCEL,
	KEY_MENU,
	KEY_PAUSE,
	KEY_UP,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_LENGTH
};

// ゲームパッド(全16ボタン)のボタン定義
enum PADBUTTON{
	PAD_DOWN,
	PAD_LEFT,
	PAD_RIGHT,
	PAD_UP,
	PAD_1,
	PAD_2,
	PAD_3,
	PAD_4,
	PAD_5,
	PAD_6,
	PAD_7,
	PAD_8,
	PAD_9,
	PAD_10,
	PAD_11,
	PAD_12,
	PAD_LENGTH
};

// マウスのボタン定義
enum GAMEMOUSE {
	MOUSE_LEFT,
	MOUSE_RIGHT,
	MOUSE_MIDDLE,
	MOUSE_LENGTH
};


/*! @brief キーコンフィグ管理クラス
入力デバイス(キーボード、ゲームパッドなど)のキーコンフィグ情報を保持するクラス。
ボタンの割当て及び、仮想ボタン入力からデバイスキー入力への変換を行う。
InputManager以外からは呼ばない。
*/
class ConfigKeyMap {
public:
	ConfigKeyMap();
	virtual ~ConfigKeyMap();

	/*! @brief マップを元に仮想ボタン入力をデバイス入力に変換する
	@param[in] configKey 仮想キー
	*/
	int getInputKey(int configKey) { return mp_configKeyMap[configKey]; }
	int getInputPad(int configPad) { return mp_configPadMap[configPad]; }

	bool loadKeyMap();
	bool loadKeyMap(const char* keymap);

	/*! @brief デバイス入力の仮想ボタンへの割当てをマップに登録する。
	@param[in] inputKey 入力キー
	@param[in] configKey 仮想キー
	*/
	bool assignKey(int inputKey, int configKey);
	bool assignPad(int inputPad, int configPad);

private:
	int* mp_configKeyMap;		//!< キーコンフィグマップ
	int* mp_configPadMap;		//!< ゲームパッドコンフィグマップ
};

};
#endif // !__INCLUDED_CONFIGKEYMAP_H__