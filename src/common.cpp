#include "common.h"

static int hDebugConsoleWindow = 0;

void createConsoleWindow()
{
	AllocConsole();
	hDebugConsoleWindow = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
	*stdout = *_fdopen(hDebugConsoleWindow, "w");
	setvbuf(stdout, NULL, _IONBF, 0);
}

void closeConsoleWindow()
{
	_close(hDebugConsoleWindow);
}