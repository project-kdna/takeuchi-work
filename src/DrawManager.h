#ifndef __INCLUDED_DRAWMANAGER_H__
#define __INCLUDED_DRAWMANAGER_H__

#include "common.h"

#include <DxLib.h>
#include <cassert>
#include <vector>

class Image
{
public:
	Image(int handle, double alpha = 255.0, int x = 0, int y = 0);
	~Image();

	void setHandle(int handle) { m_handle = handle; }
	void setPosition(int x, int y) { m_x = x; m_y = y; }
	void setSize(int width, int height) { m_width = width; m_height = height; }
	void setAlpha(double alpha) { m_alpha = alpha; }

	int getHandle() { return m_handle; }
	int getX() { return m_x; }
	int getY() { return m_y; }
	void getPosition(int* x, int* y) { x = &m_x; y = &m_y; }
	double getAlpha() { return m_alpha; }

	void draw();

private:
	int m_handle;				//!< 画像ハンドル
	int m_x, m_y;				//!< 座標				
	int m_width, m_height;		//!< サイズ
	double m_alpha;				//!<透過度
};

// 無名名前空間
namespace {

	class Layer
	{
	public:
		Layer();
		~Layer();

		void append(Image* img);
		void remove(Image* img);
		void clear();

		void draw();

		void setAlpha(double opacity) { m_alpha = opacity; }
		void setIsVisible(bool isVisible) { m_isVisible = isVisible; }

		double getAlpha() { return m_alpha; }
		bool getIsVisible() { return m_isVisible; }

	private:
		std::vector<Image*> mp_images;
		typedef std::vector<Image*>::iterator ImgItr;
		double m_alpha;
		bool m_isVisible;
	};
}

#define MAX_LAYER_NUM 32

/*! @brief 描画管理クラス */
class DrawManager
{
public:

	static DrawManager* getInstance()
	{
		if (!mp_inst) { mp_inst = new DrawManager(); }
		return mp_inst;
	}
	static void deleteInstance()
	{
		delete mp_inst; mp_inst = 0;
	}
	virtual ~DrawManager();

	void append(Image* img, int layerIdx);
	void remove(Image* img, int layerIdx);
	void clear(int layerIdx);
	void clearAll();

	void draw();


	double getLayerAlpha(int layerIdx)
	{
		assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");
		return mp_layers[layerIdx]->getAlpha();
	}

	bool getLayerIsVisible(int layerIdx)
	{
		assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");
		mp_layers[layerIdx]->getIsVisible();
	}

	void setLayerAlpha(int layerIdx, double alpha)
	{
		assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");
		mp_layers[layerIdx]->setAlpha(alpha);
	}


	void setLayerIsVisible(int layerIdx, bool isVisible)
	{
		assert((layerIdx >= 0 || layerIdx < MAX_LAYER_NUM) && "範囲外です\n");
		mp_layers[layerIdx]->setIsVisible(isVisible);
	}

private:
	std::vector<Layer*> mp_layers;						//!< レイヤーリスト
	typedef std::vector<Layer*>::iterator LayerItr;

	static DrawManager* mp_inst;						//!<描画マネージャインスタンス

	DrawManager();

	DrawManager(const DrawManager& c);				// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)
	DrawManager& operator =(const DrawManager& c);	// コピーコンストラクタ禁止(未定義->呼び出したらリンカエラー)

};

#endif // !__INCLUDED_DRAWMANAGER_H__
