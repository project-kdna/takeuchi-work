#include "ConfigKeyMap.h"
using namespace kdna;

/*! @brief コンストラクタ
*/
ConfigKeyMap::ConfigKeyMap(){
	mp_configKeyMap = new int[KEY_LENGTH];
	mp_configPadMap = new int[PAD_LENGTH];

	for (int i = 0; i < KEY_LENGTH; i++) { mp_configKeyMap[i] = 0; }
	for (int i = 0; i < PAD_LENGTH; i++) { mp_configPadMap[i] = 0; }
}


/*! @brief デストラクタ
*/
ConfigKeyMap::~ConfigKeyMap(){
	delete[] mp_configKeyMap;
	delete[] mp_configPadMap;
}


/*! @brief デフォルトのキーを割り当てる
*/
bool ConfigKeyMap::loadKeyMap(){
	assignKey(KEY_INPUT_Z, KEY_OK);
	assignKey(KEY_INPUT_X, KEY_CANCEL);
	assignKey(KEY_INPUT_C, KEY_MENU);
	assignKey(KEY_INPUT_SPACE, KEY_PAUSE);
	assignKey(KEY_INPUT_UP, KEY_UP);
	assignKey(KEY_INPUT_DOWN, KEY_DOWN);
	assignKey(KEY_INPUT_LEFT, KEY_LEFT);
	assignKey(KEY_INPUT_RIGHT, KEY_RIGHT);
	
	assignPad(PAD_2, KEY_OK);
	assignPad(PAD_3, KEY_CANCEL);
	assignPad(PAD_1, KEY_MENU);
	assignPad(PAD_10, KEY_PAUSE);
	assignPad(PAD_UP, KEY_UP);
	assignPad(PAD_DOWN, KEY_DOWN);
	assignPad(PAD_LEFT, KEY_LEFT);
	assignPad(PAD_RIGHT, KEY_RIGHT);

	return true;
}


/*! @brief ファイル(keymap)からキーを割り当てる
@param[in] keymap キーマップコンフィグのバイナリデータ
*/
bool ConfigKeyMap::loadKeyMap(const char* keymap){
	for (int i = 0; i < KEY_LENGTH; i++){
		assignKey(keymap[i], i);
		assignPad(keymap[i+KEY_LENGTH], i);
	}
	return true;
}


/*! @brief 入力キーに対して割り当てるキーを設定する
@param[in] inputKey 入力キー
@param[in] configKey 割り当てるキー
*/
bool ConfigKeyMap::assignKey(int inputKey, int configKey)
{
	if ((inputKey < 0 || inputKey > 255)) { return false; }
	if ((configKey < 0 || configKey >(int)KEY_LENGTH)) { return false; }

	mp_configKeyMap[configKey] = inputKey;
	OutputDebugStringF("キーマップ設定しました\n");
	return true;
}


/*! @brief ゲームパッドの入力ボタンに対して割り当てるキーを設定する
@param[in] inputPad 入力キー
@param[in] configPad 割り当てるキー
*/
bool ConfigKeyMap::assignPad(int inputPad, int configPad)
{
	if ((inputPad < 0 || inputPad > PAD_LENGTH)) { return false; }
	if ((configPad < 0 || configPad >(int)KEY_LENGTH)) { return false; }

	mp_configPadMap[configPad] = inputPad;
	OutputDebugStringF("キーマップ設定しました\n");
	return true;
}
