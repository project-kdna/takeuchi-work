#include "TitleScene.h"

using namespace kdna;
/*! @brief コンストラクタ */
TitleScene::TitleScene(Scene* parent) : Scene(parent)
{
	// シーンを有効にする
	validate();
}

/*! @brief デストラクタ */
TitleScene::~TitleScene()
{
	// 子供シーンを全て削除する
	destroy();
}


/*! @brief 初期化する */
void TitleScene::initialize()
{
	printf("TitleScene: 生成\n");

	if (mp_chara == nullptr) {
		mp_chara = new Image(LoadGraph("data/player.bmp"), 255.0, 15, 15);
	}

}

/*! @brief シーンを実行する */
Scene* TitleScene::run()
{
	int message = EVT::NONE;

	InputManager &imgr = InputManager::getInstance();

	if (imgr.getConfigKeyState(KEY_UP) != 0) { message = EVT::QUIT; }

	DrawManager* dmgr = DrawManager::getInstance();
	dmgr->append(mp_chara, 1);

	return mp_parent->receiveMessage(this, message);
}

/*! @brief 描画する */
void TitleScene::draw()
{
	DrawManager* dmgr = DrawManager::getInstance();
	dmgr->append(mp_chara, 1);
}