#ifndef __INCLUDED_TITLESCENE_H__
#define __INCLUDED_TITLESCENE_H__

#include "InputDevice.h"
#include "DrawManager.h"
#include "IScene.h"
#include "common.h"

class TitleScene :
	public IScene
{
public:
	TitleScene(IScene* parent) : IScene(parent)
	{
		OutputDebugStringF("TitleScene: 生成\n");

		// 入力デバイスの取得
		mp_idev = InputDevice::getInstance();
		// キーアサイン
		mp_idev->assign(KEY_INPUT_ESCAPE, KEY_SELECT);
		mp_idev->assign(KEY_INPUT_RETURN, KEY_START);
		mp_idev->assign(KEY_INPUT_UP, KEY_UP);
		mp_idev->assign(KEY_INPUT_DOWN, KEY_DOWN);

		// 描画マネージャの取得
		mp_drawMan = new DrawManager();
		mp_drawMan->getLayer();

		m_frames = 0;
		m_prevFrames = 0;
		m_isAnimate = false;
	
	}
	virtual ~TitleScene() { OutputDebugStringF("TitleScene: 消滅\n"); }

	virtual IScene* update()
	{
		Event e = NONE;
		if (!m_isValid) { initialize(); m_isValid = true; }
		mp_idev->updateKeyboard();

		if (mp_idev->getConfigKeyState(KEY_SELECT) != 0) { e = QUIT; }

		// デモ
		if (m_frames < 255) {
			if (!m_isAnimate) { m_prevFrames = m_frames; m_isAnimate = true; }
			int diff = (int)(m_frames - m_prevFrames);
			SetDrawBright(diff, diff, diff);
		}



		m_frames++;

		return mp_parent->receiveMessage(this, e);
	}
	virtual void draw() {}

private:
	InputDevice* mp_idev;			//!< 入力デバイス
	DrawManager* mp_drawMan;			//!< 描画マネージャ

	unsigned long int m_frames;		//!< フレームカウンタ(最大828.5時間)
	unsigned long int m_prevFrames;	//!< イベント開始フレーム
	bool m_isAnimate;				//!< 現在アニメーション状態化

	void initialize()
	{
	}

};


#endif // !__INCLUDED_TITLESCENE_H__
